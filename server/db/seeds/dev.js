/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */

const { faker } = require('@faker-js/faker');

const createFakeUser = () => ({
  email: faker.internet.email(),
  firstName: faker.name.firstName(),
});

exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('users').del()
  let users = [];
  for (let i = 0; i < 5; i++) {
    users.push(createFakeUser());
  }
  await knex('users').insert(users);
};
