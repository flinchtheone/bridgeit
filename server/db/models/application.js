const { Model } = require('objection');

class Application extends Model {
    static get tableName() {
        return 'applications';
    }
}
module.exports = Application