/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
// TODO assets, negatives would be separate table.
exports.up = function(knex) {
    return knex.schema.createTable('users', (table) => {
        table.increments();
        table.string('first_name').notNullable();
        table.string('last_name').notNullable();
        table.string('location').notNullable();
        table.date('dob').notNullable();
        table.string('photo_id');
        table.integer('quarter_salary');
        table.integer('credit_card');
        table.integer('home_loan');
        table.integer('bank_balance');
        table.string('ticker'); // TODO Ticker fields would be json or sep table
        table.integer('number_of_ticker');
        table.timestamps(true, true);
    })
    .createTable('applications', (table) => {
        table.increments();
        table.integer('status').notNullable(); // TODO This would be delared as an enum somewhere
        table.integer('user_id').references('id').inTable('users');
        table.timestamps(true, true);
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.raw('DROP TABLE IF EXISTS users CASCADE;DROP TABLE IF EXISTS applications CASCADE;')
};
