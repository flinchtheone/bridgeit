require("dotenv").config();
const dbSetup = require('./db/db-setup');
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const User = require('./db/models/user');
const Application = require('./db/models/application');
const axios = require("axios");

dbSetup();
  
const app = express();

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
const port = process.env.PORT;
app.listen(port, () => {
    console.log(`server is up and listening on port ${port}`);
})


app.post('/api/v1/application-onboarding/step1', async(req, res) => {
    try {
    const { first_name: firstName, last_name: lastName, dob, location, file_2: file } = req.body;

    const user = await User.query().insert({first_name: firstName, last_name: lastName, dob: dob, photo_id: file[0].name, location: location });
    const application = await Application.query().insert({user_id: user.id, status: 0 });    
    if(user && application) {
        res.status(201).json({
            status: "success",
            data: { ...user }
        })}
    } catch(err) {
        res.json(err);
    }
} )

app.post('/api/v1/application-onboarding/complete', async(req, res) => {
    try {

    const { stock_count: stockCount, credit_card_debt: creditCardDebt } = req.body;
    const tickerPriceResponse = await axios.get(`https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=${req.body.stock_ticker}&apikey=${process.env.ALPHA_API}`)
    const tickerPrice = tickerPriceResponse.data['Global Quote']['05. price']
    let approved = false;
    
    if ((parseInt(stockCount) * parseInt(tickerPrice)) > parseInt(creditCardDebt)) {
        approved = true;
    }
    res.status(201).json({
        status: "success",
        data: { approved }
    })} catch(err) {
        res.json(err);
    }
} )